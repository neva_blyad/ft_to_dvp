//////////////////////////////////////////////////////////////////////////////////
// Artem Korotchenko
// a.korotchenko@korda-group.ru
//////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns / 1 ps

// DVP output.
//
// TODO:
//  - add static asserts on module parameters,
//  - add support for zero-length timings?
module dvp_output
#(
    // DVP output timings
    parameter VSYNC_WIDTH                   =  5, // VSYNC pulse width is 5 clocks
`ifdef USE_HSYNC
    parameter VSYNC_AND_HSYNC_BETWEEN_WIDTH = 10, // Width between the first VSYNC and the first HSYNC is 10 clocks
    parameter HSYNC_WIDTH                   =  2, // HSYNC pulse width is 2 clocks
    parameter HSYNC_AND_HREF_BETWEEN_WIDTH  =  2, // Width between HSYNC and HREF is 2 clocks
    parameter HREF_AND_HSYNC_BETWEEN_WIDTH  =  2, // Width between HREF and HSYNC is 2 clocks
`else
    parameter VSYNC_AND_HREF_BETWEEN_WIDTH  = 10, // Width between the first VSYNC and the first HREF is 10 clocks
    parameter HREF_AND_HREF_BETWEEN_WIDTH   =  2, // Width between two HREFs is 2 clocks
`endif
    parameter HREF_AND_VSYNC_BETWEEN_WIDTH  = 10, // Width between the last HREF and the second VSYNC is 10 clocks

    // I/O dimensions
    parameter DATA_WIDTH     = 10, // Number of pins in data_in wire
    parameter DATA_LEN_WIDTH = 16  // Number of pins in data_len wire
)
(
    // System
    input  wire clk,
    input  wire rst_n,
    output reg  err,

    // Data input
    input  wire [DATA_WIDTH - 1:0]     data_in,
    input  wire                        data_empty,
`ifdef WAIT_FOR_ENTIRE_LINE_ARRIVED_IN_FIFO
    input  wire [DATA_LEN_WIDTH - 1:0] data_len,
`endif
    output reg                         data_next,

    // DVP
    output reg  [DATA_WIDTH - 1:0] DATA,
    output reg                     VSYNC,
`ifdef USE_HSYNC
    output reg                     HSYNC,
`endif
    output reg                     HREF,
    output wire                    PCLK
);

// Image DATA FSM states
localparam RECV_LINES_LSB    = 3'b000;
localparam RECV_LINES_MSB    = 3'b001;
localparam RECV_LINE_LEN_LSB = 3'b010;
localparam RECV_LINE_LEN_MSB = 3'b011;
localparam RDY               = 3'b100;

// FSM states
`ifdef USE_HSYNC
localparam IDLE                    = 4'b0000;
localparam VSYNC_FIRST             = 4'b0001;
localparam VSYNC_AND_HSYNC_BETWEEN = 4'b0010;
localparam HSYNC_                  = 4'b0011;
localparam HSYNC_AND_HREF_BETWEEN  = 4'b0100;
localparam HREF_                   = 4'b0101;
localparam HREF_AND_HSYNC_BETWEEN  = 4'b0110;
localparam HREF_AND_VSYNC_BETWEEN  = 4'b0111;
localparam VSYNC_SECOND            = 4'b1000;
`else
localparam IDLE                    =  3'b000;
localparam VSYNC_FIRST             =  3'b001;
localparam VSYNC_AND_HREF_BETWEEN  =  3'b010;
localparam HREF_                   =  3'b011;
localparam HREF_AND_HREF_BETWEEN   =  3'b100;
localparam HREF_AND_VSYNC_BETWEEN  =  3'b101;
localparam VSYNC_SECOND            =  3'b110;
`endif

// Variables
reg [ 2:0] state_data;
reg [15:0] lines;
reg [15:0] line_len;
`ifdef USE_HSYNC
reg [ 3:0] state;
`else
reg [ 2:0] state;
`endif
reg [ 7:0] ticks;

// Clock output
assign PCLK = clk & state != IDLE;

// Main FSM process
generate

always @(posedge clk or
         negedge rst_n)

// Handle reset pin
if (!rst_n)
begin
    err       <= 1'b0;
    data_next <= 1'b0;

    DATA  <= 0;
    VSYNC <= 1'b0;
`ifdef USE_HSYNC
    HSYNC <= 1'b1;
`endif
    HREF  <= 1'b0;

    state_data <= RECV_LINES_LSB;
    lines      <= 0;
    line_len   <= 0;
    state      <= IDLE;
    ticks      <= 0;
end
else
begin
    // Handle clock tick.
    //
    // Image data FSM.
    case (state_data)

    RECV_LINES_LSB:
    begin
        if (data_empty)
            data_next <= 1'b0;
        else
        begin
            data_next  <= 1'b1;
            state_data <= RECV_LINES_MSB;
            lines[7:0] <= data_in;
        end
    end

    RECV_LINES_MSB:
    begin
        if (data_empty)
            data_next <= 1'b0;
        else
        begin
            data_next   <= 1'b1;
            state_data  <= RECV_LINE_LEN_LSB;
            lines[15:8] <= data_in;

            if ((lines[7:0] | data_in) == 0)
                err <= 1'b1;
        end
    end

    RECV_LINE_LEN_LSB:
    begin
        if (data_empty)
            data_next <= 1'b0;
        else
        begin
            data_next     <= 1'b1;
            state_data    <= RECV_LINE_LEN_MSB;
            line_len[7:0] <= data_in;
        end
    end

    RECV_LINE_LEN_MSB:
    begin
        if (data_empty)
            data_next <= 1'b0;
        else
        begin
            data_next      <= 1'b1;
            state_data     <= RDY;
            line_len[15:8] <= data_in;

            if ((line_len[7:0] | data_in) == 0)
                err <= 1'b1;
        end
    end

    RDY:
    begin
        data_next <= 1'b0;
    end

    endcase

    // FSM
    case (state)

    IDLE:
    begin
        if ((state_data == RECV_LINE_LEN_MSB && !data_empty) ||
             state_data == RDY)
        begin
            VSYNC <= 1'b1;
            state <= VSYNC_FIRST;
        end
    end

    VSYNC_FIRST:
    begin
        if (ticks == VSYNC_WIDTH - 1)
        begin
            VSYNC <= 1'b0;
`ifdef USE_HSYNC
            state <= VSYNC_AND_HSYNC_BETWEEN;
`else
            state <= VSYNC_AND_HREF_BETWEEN;
`endif
            ticks <= 0;
        end
        else
            ticks <= ticks + 8'b1;
    end

`ifdef USE_HSYNC
    VSYNC_AND_HSYNC_BETWEEN:
    begin
        if (ticks == VSYNC_AND_HSYNC_BETWEEN_WIDTH - 1)
        begin
            HSYNC <= 1'b0;
            state <= HSYNC_;
            ticks <= 0;
        end
        else
            ticks <= ticks + 8'b1;
    end

    HSYNC_:
    begin
        if (ticks == HSYNC_WIDTH - 1)
        begin
            HSYNC <= 1'b1;
            state <= HSYNC_AND_HREF_BETWEEN;
            ticks <= 0;
        end
        else
            ticks <= ticks + 8'b1;
    end

    HSYNC_AND_HREF_BETWEEN:
    begin
        if (ticks == HSYNC_AND_HREF_BETWEEN_WIDTH - 1)
        begin
            if (state_data == RDY)
            begin
`ifdef WAIT_FOR_ENTIRE_LINE_ARRIVED_IN_FIFO
                if (data_len < line_len)
`else
                if (data_empty)
`endif
                    ;
                else
                begin
                    data_next <= 1'b1;
                    DATA      <= data_in;
                    HREF      <= 1'b1;
                    line_len  <= line_len - 16'b1;
                    state     <= HREF_;
                    ticks     <= 0;
                end
            end
        end
        else
            ticks <= ticks + 8'b1;
    end
`else
    VSYNC_AND_HREF_BETWEEN:
    begin
        if (ticks == VSYNC_AND_HREF_BETWEEN_WIDTH - 1)
        begin
`ifdef WAIT_FOR_ENTIRE_LINE_ARRIVED_IN_FIFO
            if (data_len < line_len)
`else
            if (data_empty)
`endif
                ;
            else
            begin
                data_next <= 1'b1;
                DATA      <= data_in;
                HREF      <= 1'b1;
                line_len  <= line_len - 16'b1;
                state     <= HREF_;
                ticks     <= 0;
            end
        end
        else
            ticks <= ticks + 8'b1;
    end
`endif

    HREF_:
    begin
        // Have any more bytes in line to send?
        if (line_len > 0)
        begin
            // Have more bytes.
            // Line has not been sent yet.
`ifdef WAIT_FOR_ENTIRE_LINE_ARRIVED_IN_FIFO
`else
            if (data_empty)
            begin
                DATA <= 0;
                HREF <= 1'b0;
            end
            else
            begin
`endif
                data_next <= 1'b1;
                if (HREF)
                    DATA <= data_in;
                line_len <= line_len - 16'b1;
`ifdef WAIT_FOR_ENTIRE_LINE_ARRIVED_IN_FIFO
`else
            end
`endif
        end
        else
        begin
            // Line has been sent
            DATA <= 0;
            HREF <= 1'b0;

            lines <= lines - 16'b1;

            // Have any more lines to send?
            if (lines > 1) // Note: test for 1, not 0, because changes to register are not applied immidiately
            begin
                // Have more lines
                if (data_empty)
                    state_data <= RECV_LINE_LEN_LSB;
                else
                begin
                    data_next     <= 1'b1;
                    state_data    <= RECV_LINE_LEN_MSB;
                    line_len[7:0] <= data_in;
                end

`ifdef USE_HSYNC
                state <= HREF_AND_HSYNC_BETWEEN;
`else
                state <= HREF_AND_HREF_BETWEEN;
`endif
            end
            else
            begin
                // All lines have been sent
                if (data_empty)
                    state_data <= RECV_LINES_LSB;
                else
                begin
                    data_next  <= 1'b1;
                    state_data <= RECV_LINES_MSB;
                    lines[7:0] <= data_in;
                end

                state <= HREF_AND_VSYNC_BETWEEN;
            end
        end
    end

`ifdef USE_HSYNC
    HREF_AND_HSYNC_BETWEEN:
    begin
        if (ticks == HREF_AND_HSYNC_BETWEEN_WIDTH - 1)
        begin
            HSYNC <= 1'b0;
            state <= HSYNC_;
            ticks <= 0;
        end
        else
            ticks <= ticks + 8'b1;
    end
`else
    HREF_AND_HREF_BETWEEN:
    begin
        if (ticks == HREF_AND_HREF_BETWEEN_WIDTH - 1)
        begin
            if (state_data == RDY)
            begin
`ifdef WAIT_FOR_ENTIRE_LINE_ARRIVED_IN_FIFO
                if (data_len < line_len)
`else
                if (data_empty)
`endif
                    ;
                else
                begin
                    data_next <= 1'b1;
                    DATA      <= data_in;
                    HREF      <= 1'b1;
                    line_len  <= line_len - 16'b1;
                    state     <= HREF_;
                    ticks     <= 0;
                end
            end
        end
        else
            ticks <= ticks + 8'b1;
    end
`endif

    HREF_AND_VSYNC_BETWEEN:
    begin
        if (ticks == HREF_AND_VSYNC_BETWEEN_WIDTH - 1)
        begin
            VSYNC <= 1'b1;
            state <= VSYNC_SECOND;
            ticks <= 0;
        end
        else
            ticks <= ticks + 8'b1;
    end

    VSYNC_SECOND:
    begin
        if (ticks == VSYNC_WIDTH - 1)
        begin
            VSYNC <= 1'b0;
            state <= IDLE;
            ticks <= 0;
        end
        else
            ticks <= ticks + 8'b1;
    end

    endcase
end

endgenerate

endmodule
