//////////////////////////////////////////////////////////////////////////////////
// Dmitry Koroteev
// korob14@gmail.com
//
// Artem Korotchenko
// a.korotchenko@korda-group.ru
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps

// Serial controller
module serial_ctrl
(
    // System
    input  wire       clk,
    input  wire       rst_n,

    // Input
    input  wire       rx_rdy,
    input  wire       tx_rdy,
    input  wire [7:0] rx,
    input  wire       rx_valid,

    // Output
    output reg        rx_en,
    output reg        tx_en,
    output reg  [7:0] tx,
    output reg  [7:0] out,
    output reg        out_valid
);

// Main process
always @(posedge clk or
         negedge rst_n)

// Handle reset pin
if (!rst_n)
begin
    rx_en     <= 1'b0;
    tx_en     <= 1'b0;
    tx        <= 0;
    out       <= 0;
    out_valid <= 1'b0;
end
else
begin
    // Handle clock tick.
    // Only receive is needed.
    rx_en <= rx_rdy;

    if (rx_valid)
    begin
        out       <= rx;
        out_valid <= 1'b1;
    end
    else
    begin
        out       <= 0;
        out_valid <= 1'b0;
    end

    // Transmission is disabled
    tx_en <= 1'b0;
    tx    <= 0;
end

endmodule
