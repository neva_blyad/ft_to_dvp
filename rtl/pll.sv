//////////////////////////////////////////////////////////////////////////////////
// Artem Korotchenko
// a.korotchenko@korda-group.ru
//////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns / 1 ps

// PLL.
//
// TODO:
//  - add static asserts on module parameters.
module pll
#(
    parameter OUT_SIZE = 3,

    parameter integer CLK_IN_FREQ_HZ                  =    50000000, // clk_in     = 50 MHz
    parameter integer CLK_OUT_FREQ_HZ[OUT_SIZE - 1:0] = '{  1000000, // clk_out[2] =  1 MHz
                                                           25000000, // clk_out[1] = 25 MHz
                                                           10000000  // clk_out[0] = 10 MHz
                                                         }
)
(
    input wire clk_in,
    input wire rst_n,
    output reg [OUT_SIZE - 1:0] clk_out
);

// Variables
integer       out;
`ifdef USE_PLL_DIV
integer       period[OUT_SIZE - 1:0];
integer       half  [OUT_SIZE - 1:0];
reg     [7:0] ticks [OUT_SIZE - 1:0];
`endif

generate

`ifdef USE_PLL_DIV
// Drive the divided output clocks
always @(posedge clk_in or
         negedge rst_n)

    // Handle reset pin
    if (!rst_n)
        for (out = 0; out < OUT_SIZE; out = out + 1)
        begin
            clk_out[out] <= clk_in;
            period [out] <= CLK_IN_FREQ_HZ / CLK_OUT_FREQ_HZ[out];
            half   [out] <= period[out] / 2;
            ticks  [out] <= 0;
        end
    else
        // Handle clock tick
        for (out = 0; out < OUT_SIZE; out = out + 1)
            if (ticks[out] == half[out] - 1)
            begin
                clk_out[out] <= ~clk_out[out];
                ticks  [out] <= 0;
            end
            else
                ticks[out] <= ticks[out] + 8'b1;
`else
// Drive the output clocks with the same rate as the input clock
always @(*)

for (out = 0; out < OUT_SIZE; out = out + 1)
    clk_out[out] = clk_in & rst_n;
`endif

endgenerate

endmodule
