//////////////////////////////////////////////////////////////////////////////////
// Dmitry Koroteev
// korob14@gmail.com
//
// Artem Korotchenko
// a.korotchenko@korda-group.ru
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps

// FT to DVP.
//
// TODO:
//  - add static asserts on module parameters.
module ft_to_dvp
#(
    // FT232 clock frequency
    parameter CLK_FREQ_HZ   = 60000000, // 60 MHz

    // FIFO settings
    parameter FIFO_SIZE     = 4 * 1024, //  4 KiB

    // DVP output timings
    parameter VSYNC_WIDTH                   =  5, // VSYNC pulse width is 5 clocks
`ifdef USE_HSYNC
    parameter VSYNC_AND_HSYNC_BETWEEN_WIDTH = 10, // Width between the first VSYNC and the first HSYNC is 10 clocks
    parameter HSYNC_WIDTH                   =  2, // HSYNC pulse width is 2 clocks
    parameter HSYNC_AND_HREF_BETWEEN_WIDTH  =  2, // Width between HSYNC and HREF is 2 clocks
    parameter HREF_AND_HSYNC_BETWEEN_WIDTH  =  2, // Width between HREF and HSYNC is 2 clocks
`else
    parameter VSYNC_AND_HREF_BETWEEN_WIDTH  = 10, // Width between the first VSYNC and the first HREF is 10 clocks
    parameter HREF_AND_HREF_BETWEEN_WIDTH   =  2, // Width between two HREFs is 2 clocks
`endif
    parameter HREF_AND_VSYNC_BETWEEN_WIDTH  = 10  // Width between the last HREF and the second VSYNC is 10 clocks
)
(
    // System
    input  wire       rst_n,
    output wire [2:0] LED,

    // FT232H UART to USB
    input  wire       CLKOUT,
    input  wire       RXF_n,
    input  wire       TXE_n,
    output wire       OE_n,
    output wire       RD_n,
    output wire       WR_n,
    output wire       SI_n,
    inout  wire [7:0] DATA,

    // DVP
    output wire [7:0] DATA_,
    output wire       VSYNC,
`ifdef USE_HSYNC
    output wire       HSYNC,
`endif
    output wire       HREF,
    output wire       PCLK
);

// PLL frequencies
localparam PCLK_FREQ_HZ = CLK_FREQ_HZ;

// Constants
localparam FIFO_ADDR_WIDTH = $clog2(FIFO_SIZE);

// Wires
wire       clk_dvp;
wire       rst_shared_n;

wire       serial_tx_en;
wire       serial_rx_en;
wire [7:0] serial_rx;
wire [7:0] serial_tx;
wire       serial_rx_rdy;
wire       serial_rx_valid;

wire [7:0] fifo_in;
wire [7:0] fifo_out;
wire       fifo_in_req;
wire       fifo_out_req;
wire       fifo_empty;
wire       fifo_full;
wire [FIFO_ADDR_WIDTH - 1:0] fifo_len;
wire       fifo_overflow;

wire       dvp_led;
wire       dvp_err;

assign fifo_overflow = fifo_full & fifo_in_req & ~CLKOUT;
assign rst_shared_n  = rst_n & !fifo_overflow & !dvp_err;

assign LED[0] = (rst_shared_n === 1'b0) ? 1'bZ : 1'b0;
assign LED[1] = (!RXF_n === 1'b0) ? 1'bZ : 1'b0;
assign LED[2] = !dvp_led;

// FT232H synchronization interface
FT_Sync ft_sync_0
(
    .clk         (CLKOUT),
    .rst_n       (rst_shared_n),
    .RXF_n       (RXF_n),
    .TXE_n       (TXE_n),
    .OE_n        (OE_n),
    .RD_n        (RD_n),
    .WR_n        (WR_n),
    .SI_n        (SI_n),
    .DATA        (DATA),
    .write_data  (serial_tx),
    .wr          (serial_tx_en),
    .rd          (serial_rx_en),
    .wr_ready    (),
    .rd_ready    (serial_rx_rdy),
    .read_data   (serial_rx),
    .data_valid  (serial_rx_valid)
);

// Serial controller
serial_ctrl serial_ctrl_0
(
    // System
    .clk       (CLKOUT),
    .rst_n     (rst_shared_n),

    // Input
    .rx_rdy    (serial_rx_rdy),
    .tx_rdy    (),
    .rx        (serial_rx),
    .rx_valid  (serial_rx_valid),

    // Output
    .rx_en     (serial_rx_en),
    .tx_en     (serial_tx_en),
    .tx        (serial_tx),
    .out       (DATA_),
    .out_valid (fifo_in_req)
);

// PLL
pll
#(
    .OUT_SIZE(1),

    .CLK_IN_FREQ_HZ(CLK_FREQ_HZ),
    .CLK_OUT_FREQ_HZ('{ PCLK_FREQ_HZ })
)
pll_0
(
    .clk_in(CLKOUT),
    .rst_n(rst_shared_n),
    .clk_out(clk_dvp)
);

// Dual-clock FIFO
dc_data_fifo
#(
    .ADDR_W(FIFO_ADDR_WIDTH)
)
fifo_0
(
    //.rdclk(clk_dvp),
    .rdclk(CLKOUT),
    .wrclk(~CLKOUT),
    .rst_n(rst_shared_n),
    .rdreq(fifo_out_req),
    .wrreq(fifo_in_req),
    .data_in(fifo_in),
    .data_out(fifo_out),
    .rdempty(fifo_empty),
    .wrfull(fifo_full),
    .rdusedw(fifo_len)
);

// DVP output
dvp_output
#(
    .VSYNC_WIDTH                   (VSYNC_WIDTH),
`ifdef USE_HSYNC
    .VSYNC_AND_HSYNC_BETWEEN_WIDTH (VSYNC_AND_HSYNC_BETWEEN_WIDTH),
    .HSYNC_WIDTH                   (HSYNC_WIDTH),
    .HSYNC_AND_HREF_BETWEEN_WIDTH  (HSYNC_AND_HREF_BETWEEN_WIDTH),
    .HREF_AND_HSYNC_BETWEEN_WIDTH  (HREF_AND_HSYNC_BETWEEN_WIDTH),
`else
    .VSYNC_AND_HREF_BETWEEN_WIDTH  (VSYNC_AND_HREF_BETWEEN_WIDTH),
    .HREF_AND_HREF_BETWEEN_WIDTH   (HREF_AND_HREF_BETWEEN_WIDTH),
`endif
    .HREF_AND_VSYNC_BETWEEN_WIDTH  (HREF_AND_VSYNC_BETWEEN_WIDTH),

    .DATA_WIDTH(8),
    .DATA_LEN_WIDTH(FIFO_ADDR_WIDTH)
)
dvp_output_0
(
    // System
    .clk        (~CLKOUT),
    .rst_n      (rst_shared_n),
    .led        (dvp_led),
    .err        (dvp_err),

    // Data input
    .data_in    (fifo_out),
    .data_empty (fifo_empty),
`ifdef WAIT_FOR_ENTIRE_LINE_ARRIVED_IN_FIFO
    .data_len   (fifo_len),
`endif
    .data_next  (fifo_out_req),

    // DVP
    .DATA       (DATA_),
    .VSYNC      (VSYNC),
`ifdef USE_HSYNC
    .HSYNC      (HSYNC),
`endif
    .HREF       (HREF),
    .PCLK       (PCLK)
);

endmodule
