# NAME

FT_to_DVP

# VERSION

0.9

# ORIGINAL WORK

This code was forked from:

DVP_to_FT
https://github.com/KoroB14/DVP_to_FT

# DESCRIPTION

Show images and stream video from USB over DVP.
Use FPGA and FT232H USB-to-UART device configured in FT245 Synchronous FIFO
mode.

                           _______________________________________________________
                          |                                                       |     __________
                          |                          FPGA                         |    |          |
         ____________     |   ____________       ____________                     |    |          |
        |            |    |  |            |     |            |                    |    |          |
        | FT232H     | 3  |  | FT232H     |  3  | Serial     |                    |    |          |
        | USB to     |------>| sync       |---->| controller |                    |    |          |
        | UART       |    |  | interface  |     |____________|                    |    |          |
        |____________|    |  |____________|          ^ |                          |    |          |
        |            |    |        ^                 | | 3                        |    |          |
        | FT232H     | 1  |        |                 | |                          |    |          |
        | clock      |-------------------------------  v                          |    | DVP      |
        | generator  |    |   ____________   |   ____________       ___________   |    | input    |
        |____________|    |  |            |  |  |            |  3  |           |  | 4  |          |
                          |  | PLL        |<--->| Dual-clock |---->| DVP       |------>|          |
                          |  |            |     | FIFO       |     | output    |  |    |          |
                          |  |____________|     |____________|     |___________|  |    |          |
                          |        |                   ^                 ^        |    |          |
                          |        |         2         |                 |        |    |          |
                          |         -------------------------------------         |    |          |
                          |                                                       |    |          |
                          |   Serial clock                          DVP clock     |    |          |
                          |   domain                                domain        |    |__________|
                          |_______________________________________________________|

1. FT232H clock, 60 MHz = 60 Mb/s
2. DVP clock
3. Image data
4. DVP frame

PCLK clock rate in output DVP frames will be the same as input FT232H clock.
Serial and DVP clock domains now are the same.
It may change in the future releases.

# SOFTWARE

Simple Python-based client for GNU/Linux is included.

Dependencies: Python 3.7+, Python Imaging Library (PIL), USB wrapper for
libusb1.
For example, under Debian GNU/Linux, Ubuntu and derivates it means you should
do:
$ sudo apt install python3 python3-pil python3-usb1

Also you need to grant user permissions to access the FT232H device and unload
ftdi_sio kernel module. Check example udev rules file in the software folder.

This script sends one single image (JPG, PNG, ...) to USB.
You can extend it to stream video, for example.

FPGA Verilog code is to be **synthesized** in Quartus II 13.0.1 by Altera
Corporation. (It's free for several target chips, for example, Cyclone II.
Download Web Edition.)
You can try porting the project to another ASIC/FPGA synthersizer.

FPGA Verilog code is to be **simulated** in Quartus Prime 21.1.0.
(It's free for simulation. Download Lite Edition.)
This EDA uses Questa Intel FPGA simulator.
You can try porting the project to another ASIC/FPGA simulator.

# HARDWARE

This design was tested on EP2C5/EP2C8 MINI BOARD based on the Altera Cyclone II
FPGA.
You can use any other ASIC/FPGA.

(It also consists of 50 MHz crystal oscillator.
This design doesn't pressume the using of second clock generator by now, but it
may need it in the future.)

Also, I'd used ShangSi MoudleCX board.
You can use any other device based on FT232H chip.

The FT245 Synchronous FIFO mode requires the external EEPROM. Configure
following settings using FT_Prog (or other software, capable to write EEPROM):

| Parameter                                     | Value              |
| ----------------------------------------------| -------------------|
| Port A -> Hardware                            | 245 FIFO           |
| Port A -> Driver                              | D2XX Direct        |
| USB String Descriptors -> Product Description | FPGA Video Stream  |

# TUNING

+ Set external clock in the top-level module.

+ Tune DVP output timings.

+ Set size of internal FIFO as big as possible to fit on chip.

+ Define USE_HSYNC macro if HSYNC pin should be used. (Default.)
  Undefine if it doesn't.

  You can enter the setting through the GUI using:

  "Assignments --> Settings --> Analysis & Synthesis Settings --> Verilog HDL
  Input --> Verilog HDL macro" (Quartus II 13.0.1) or

  "Assignments --> Settings --> Compiler Settings --> Verilog HDL Input -->
  Verilog HDL macro" (Quartus Prime 21).

  TODO: set this macro in Questa simulator (don't know how), it's disabled.

+ Define WAIT_FOR_ENTIRE_LINE_ARRIVED_IN_FIFO macro if PCLK output clock is
  faster then UART baudrate. Undefine if it doesn't.

+ Connect LED outputs if you have LEDs on your board. Otherwise, do not connect
  them.

      0. LED[0] lights on if board is powered up,
                it lights off if there are no power or reset is active
      1. LED[1] lights on if serial data is coming
      2. LED[2] lights on if DVP module is currently outputing frame

  These LED pins used as Open-Drain outputs.

# COPYRIGHT AND LICENSE

    Copyright (C) 2021 Dmitry Koroteev <korob14@gmail.com>
    Copyright (C) 2022 Артём Коротченко <a.korotchenko@korda-group.ru>,
                       Корда Групп

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# AUTHORS

    Dmitry Koroteev <korob14@gmail.com>
    Артём Коротченко <a.korotchenko@korda-group.ru>, Корда Групп
