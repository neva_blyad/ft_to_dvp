`timescale 1 ns / 1 ps

// Testbench
module testbench();

// Constants
localparam CLK_FREQ_HZ = 60000000; // 60 MHz

// Variables
reg        rst_n;

reg        RXF_n;
reg        TXE_n;
reg [7:0]  DATA_TMP;

// Wires
wire       CLKOUT;
wire       OE_n;
wire       RD_n;
wire       WR_n;
wire       SI_n;
wire [7:0] DATA = DATA_TMP;

wire [7:0] DATA_;
wire       VSYNC;
`ifdef USE_HSYNC
wire       HSYNC;
`endif
wire       HREF;
wire       PCLK;

// FT to DVP
ft_to_dvp
#(
    .CLK_FREQ_HZ(CLK_FREQ_HZ)
)
ft_to_dvp_0
(
    // System
    .rst_n(rst_n),

    // FT232H UART
    .CLKOUT(CLKOUT),
    .RXF_n(RXF_n),
    .TXE_n(TXE_n),
    .OE_n(OE_n),
    .RD_n(RD_n),
    .WR_n(WR_n),
    .SI_n(SI_n),
    .DATA(DATA),

    // DVP
    .DATA_(DATA_),
    .VSYNC(VSYNC),
`ifdef USE_HSYNC
    .HSYNC(HSYNC),
`endif
    .HREF(HREF),
    .PCLK(PCLK)
);

// Clock generator.
//
// Generates UART baudrate square waves.
clk_gen
#(
    .FREQ_HZ(CLK_FREQ_HZ)
)
clk_gen_0
(
    .clk(CLKOUT)
);

// Initialize
initial 
begin
    $display($time, " << Starting Simulation >> ");

    #0 // First cycle

    rst_n    = 1'b0;

    RXF_n    = 1'b1;
    TXE_n    = 1'b1;
    DATA_TMP = 8'b11111111; // 0xFF (dummy)

    #6.666

    rst_n = 1'b1;

    // FIRST IMAGE.
    //
    // One line.

    #10 // Second cycle

    RXF_n = 1'b0;

    #16.666 // Third cycle

    // FT232H is not ready to send data yet

    #16.666 // Fourth cycle
    DATA_TMP = 8'b00000001; // 0x01 (lines LSB)

    #16.666 // Fifth cycle

    // Do nothing for one tick according to the FT232H datasheet

    #16.666 // ...
    DATA_TMP = 8'b00000000; // 0x00 (lines MSB)

    // lines = 0x0001

    // First line

    #16.666
    DATA_TMP = 8'b00001000; // 0x08 (line_cnt LSB)

    #16.666
    DATA_TMP = 8'b00000000; // 0x00 (line_cnt MSB)

    // line_cnt = 0x0008

    #16.666
    DATA_TMP = 8'b00000110; // 0x06

    #16.666
    DATA_TMP = 8'b00000000; // 0x00

    #16.666
    DATA_TMP = 8'b00000100; // 0x04

    #16.666
    DATA_TMP = 8'b00000101; // 0x05

    #16.666
    DATA_TMP = 8'b00000110; // 0x06

    #16.666
    DATA_TMP = 8'b00000111; // 0x07

    #16.666
    DATA_TMP = 8'b00000001; // 0x01

    #16.666
    DATA_TMP = 8'b00000010; // 0x02

    // SECOND IMAGE.
    //
    // Two lines.

    #16.666
    DATA_TMP = 8'b00000010; // 0x02 (lines LSB)

    #16.666
    DATA_TMP = 8'b00000000; // 0x00 (lines MSB)

    // lines = 0x0002

    // First line

    #16.666
    DATA_TMP = 8'b00001000; // 0x08 (line_cnt LSB)

    #16.666
    DATA_TMP = 8'b00000000; // 0x00 (line_cnt MSB)

    // line_cnt = 0x0008

    #16.666
    DATA_TMP = 8'b00000110; // 0x06

    #16.666
    DATA_TMP = 8'b00000000; // 0x00

    #16.666
    DATA_TMP = 8'b00000100; // 0x04

    #16.666
    DATA_TMP = 8'b00000101; // 0x05

    #16.666
    DATA_TMP = 8'b00000110; // 0x06

    #16.666
    DATA_TMP = 8'b00000111; // 0x07

    #16.666
    DATA_TMP = 8'b00000001; // 0x01

    #16.666
    DATA_TMP = 8'b00000010; // 0x02

    // Second line

    #16.666
    DATA_TMP = 8'b00001000; // 0x08 (line_cnt LSB)

    #16.666
    DATA_TMP = 8'b00000000; // 0x00 (line_cnt MSB)

    // line_cnt = 0x0006

    #16.666
    DATA_TMP = 8'b00000110; // 0x06

    #16.666
    DATA_TMP = 8'b00000000; // 0x00

    #16.666
    DATA_TMP = 8'b00000100; // 0x04

    #16.666
    DATA_TMP = 8'b00000101; // 0x05

    #16.666
    DATA_TMP = 8'b00000110; // 0x06

    #16.666
    DATA_TMP = 8'b00000111; // 0x07

    #16.666
    DATA_TMP = 8'b00000001; // 0x01

    #16.666
    DATA_TMP = 8'b00000010; // 0x02

    #16.666
    RXF_n    = 1'b1;
    DATA_TMP = 8'b00000000; // 0x00

    #5000
    ;

    $display($time, " << Simulation Complete >>");
    $stop;
end

endmodule
