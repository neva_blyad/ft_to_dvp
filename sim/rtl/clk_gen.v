//////////////////////////////////////////////////////////////////////////////////
// Artem Korotchenko
// a.korotchenko@korda-group.ru
//////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns / 1 ps

// Clock generator
module clk_gen
#(
    parameter FREQ_HZ = 1000000 // 1 MHz
)
(
    output reg clk
);

// Constants
localparam PERIOD_NS       = 1.0 / FREQ_HZ * 1e9;
localparam PULSE_WIDTH_ON  = 0.5 * PERIOD_NS;
localparam PULSE_WIDTH_OFF = PULSE_WIDTH_ON;

generate

// Initialize
initial 
clk = 1'b1;

// Drive the clock
always 
begin
    #(PULSE_WIDTH_ON)  clk = 1'b0;
    #(PULSE_WIDTH_OFF) clk = 1'b1;
end

endgenerate

endmodule
