#!/usr/bin/env python3
# coding: utf-8
#
# Artem Korotchenko
# a.korotchenko@korda-group.ru

import argparse
from ctypes import *
from PIL import Image
import usb1

def main():

    # parse arguments
    parser = argparse.ArgumentParser(
        description = "Send specified image file over FT232H USB"
    )
    parser.add_argument(
        "-i",
        "--input",
        required = True,
        dest = "input_file",
        help = "Input file to be sent. (PNG, JPG are tested and supported)."
    )

    args = parser.parse_args()

    # read image file
    img = Image.open(args.input_file)

    width_in, height_in = img.size

    width_out  = width_in
    height_out = height_in

    data_in  = img.getdata()
    data_out = bytearray(2 +                        # number of lines
                         height_out * 2 +           # line lengths
                         width_out * height_out * 2 # RGB565
                        )

    # add number of lines
    data_out[0] = height_out &  0xFF
    data_out[1] = height_out >> 8

    # iterate over the pixels.
    # prepare outcoming byte array.
    cnt_out = 2

    for cnt_in, pixel_in in enumerate(data_in):
        if width_out == width_in:
            data_out[cnt_out    ] = width_out &  0xFF
            data_out[cnt_out + 1] = width_out >> 8

            cnt_out   += 2
            width_out  = 1
        else:
            width_out += 1

        r = pixel_in[0] >> 3
        g = pixel_in[1] >> 2
        b = pixel_in[2] >> 3

        rgb = r << 11 | g << 5 | b

        data_out[cnt_out    ] = rgb & 0xFF
        data_out[cnt_out + 1] = rgb > 8

        cnt_out   += 2

    # find FT232H USB device
    init_str = "FPGA Video Stream"
    handle   = None
    context  = usb1.USBContext()

    id_vendor  = 0x0403
    id_product = 0x6014

    for device in context.getDeviceIterator(skip_on_error=True):
        if (device.getVendorID() == id_vendor and device.getProductID() == id_product):
            handle = device.open()

            if (handle.getProduct() == init_str): # check device name
                break

            handle.close()
            handle = None

    if (handle is None):
        print ("Failed to open device")
        return

    # send prepared array over FT232H USB
    handle.claimInterface(0)

    handle._controlTransfer(0x40, 0x09, 0x0010, 0x01, None, 0, 1000) # set latency timer
    handle._controlTransfer(0x40, 0, 2, 0x01, None, 0, 1000) # tcireset
    handle._controlTransfer(0x40, 0, 1, 0x01, None, 0, 1000) # tcoreset
    handle._controlTransfer(0x40, 0x0B, 0x00FF, 0x01, None, 0, 1000) # reset MPSSE
    handle._controlTransfer(0x40, 0x0B, 0x40FF, 0x01, None, 0, 1000) # sync fifo out

    handle.bulkWrite(0x02, data_out, 1000) # send

if __name__ == '__main__':
    main()

